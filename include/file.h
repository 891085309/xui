#pragma once

#include <sys/types.h>

int file_open(const char * path, const char * mode);
int file_close(int fd);
int file_isdir(const char * path);
int file_isfile(const char * path);
int file_mkdir(const char * path);
int file_remove(const char * path);
int file_access(const char * path, const char * mode);
void file_walk(const char * path, void (*cb)(const char * dir, const char * name, void * data), const char * dir, void * data);
ssize_t file_read(int fd, void * buf, size_t count);
ssize_t file_read_nonblock(int fd, void * buf, size_t count);
ssize_t file_write(int fd, const void * buf, size_t count);
int64_t file_seek(int fd, int64_t offset);
int64_t file_tell(int fd);
int64_t file_length(int fd);
