#ifndef __X64_TYPES_H__
#define __X64_TYPES_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>
#include <stdint.h>
#include <stddef.h>

typedef signed char				s8_t;
typedef unsigned char			u8_t;

typedef signed short			s16_t;
typedef unsigned short			u16_t;

typedef signed int				s32_t;
typedef unsigned int			u32_t;

typedef signed long long		s64_t;
typedef unsigned long long		u64_t;

typedef signed int				bool_t;
typedef unsigned long			irq_flags_t;

typedef unsigned long long		virtual_addr_t;
typedef unsigned long long		virtual_size_t;
typedef unsigned long long		physical_addr_t;
typedef unsigned long long		physical_size_t;

typedef struct {
	volatile int counter;
} atomic_t;

typedef struct {
	volatile int lock;
} spinlock_t;

enum {
	FALSE				= 0,
	TRUE				= 1,
};

#ifdef __cplusplus
}
#endif

#endif /* __X64_TYPES_H__ */
