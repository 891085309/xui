#ifndef __XBOOT_H__
#define __XBOOT_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <endian.h>
#include <types.h>
#include <defines.h>
#include <limits.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <locale.h>
#include <time.h>
#include <math.h>
#include <spring.h>
#include <malloc.h>
#include <charset.h>
#include <shash.h>
#include <list.h>
#include <hmap.h>
#include <xboot/ktime.h>
#include <xboot/window.h>
#include <xboot/event.h>
#include <xui/xui.h>
#include <xui/window.h>
#include <xui/popup.h>
#include <xui/panel.h>
#include <xui/collapse.h>
#include <xui/tree.h>
#include <xui/button.h>
#include <xui/checkbox.h>
#include <xui/radio.h>
#include <xui/toggle.h>
#include <xui/tabbar.h>
#include <xui/slider.h>
#include <xui/number.h>
#include <xui/textedit.h>
#include <xui/colorpicker.h>
#include <xui/glass.h>
#include <xui/image.h>
#include <xui/icon.h>
#include <xui/badge.h>
#include <xui/progress.h>
#include <xui/radialbar.h>
#include <xui/spinner.h>
#include <xui/split.h>
#include <xui/label.h>
#include <xui/text.h>

#ifdef __cplusplus
}
#endif

#endif /* __XBOOT_H__ */
