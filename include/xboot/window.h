#ifndef __WINDOW_H__
#define __WINDOW_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <types.h>
#include <stdint.h>
#include <list.h>
#include <fifo.h>
#include <xboot/event.h>
#include <graphic/surface.h>
#include <graphic/region.h>

struct window_t {
	struct task_t * task;
	struct list_head list;
	struct window_manager_t * wm;
	struct surface_t * s;
	struct region_list_t * rl;
	struct fifo_t * event;
	struct hmap_t * map;
	void *context;
};

static inline int window_get_width(struct window_t * w)
{
	if(w)
		return w->s->width;
	return 0;
}

static inline int window_get_height(struct window_t * w)
{
	if(w)
		return w->s->height;
	return 0;
}

struct window_t * window_alloc(int width, int height);
void window_free(struct window_t * w);
void window_to_front(struct window_t * w);
void window_to_back(struct window_t * w);
void window_region_list_add(struct window_t * w, struct region_t * r);
void window_region_list_clear(struct window_t * w);
void window_present(struct window_t * w, void * o, void (*draw)(struct window_t *, void *));
void window_exit(struct window_t * w);
int window_pump_event(struct window_t * w, struct event_t * e);
void push_event(struct event_t * e);

#ifdef __cplusplus
}
#endif

#endif /* __WINDOW_H__ */
