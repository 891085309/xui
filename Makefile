sinclude scripts/env.mk

W_FLAGS		= -Wall -Werror=implicit-function-declaration -Wno-unused-function -Werror=return-type -Wno-unused-but-set-variable -Wno-unused-variable
X_CFLAGS	+= -std=gnu11 -O3 -g -ggdb $(W_FLAGS) $(shell pkg-config --cflags cairo freetype2 libpng16 sdl2)

X_INCS		+= include/config/autoconf.h
X_INCDIRS	+= include platform/x64/include
X_LDFLAGS	+= $(shell pkg-config --libs cairo freetype2 libpng16 sdl2) -lm
NAME		:= xui-overview
SRC			+= overview.c graphic/*.c xui/*.c \
				lib/*.c lib/libx/*.c \
				driver/*.c platform/x64/ platform/cairo/