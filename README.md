# XUI

![overview](/docs/overview.png)

## 环境配置

```bash
# for Ubuntu
sudo apt install libcairo2-dev libpng-dev libsdl2-dev libfreetype-dev
```

## 编译&运行&清除

```bash
make -j8 && ./xui-overview
make clean
```
